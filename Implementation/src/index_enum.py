"""
    All enums relaterd to index enum
"""

import enum

# extend it with graphQL types


# Name of indexes
class Indexes(enum.Enum):
    """ All possible indexes """

    COFFEE = 'coffee'
    RESTAURANT = 'restaurant'
    PUB = 'pub'
    SCHOOL_ELEMENTARY = 'school_elementary'
    SCHOOL_MIDDLE = 'school_middle'
    SCHOOL_HIGH = 'school_high'
    FITNESS = 'fitness'


# cluster type
class IndexClusterTypes(enum.Enum):
    """ Cluster types """
    NEAR = 'near'
    MEDIUM = 'medium'
    FAR = 'far'


# destination by some transports
class TransportTypes(enum.Enum):
    """ Type of transport """
    WALK = 'walk'
    CAR = 'car'
