# !/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Import data from OSM to csv
"""
import os
import sys
import csv
import re
import requests
import enum4
import logging
from logni import log
from geopy.geocoders import Nominatim


DATA_LIMIT = 10
logging.basicConfig(level=logging.DEBUG)

DATA = [{'name': str(),\
		'description': str(),\
		'osm_way_id': int(),\
		'osm_node_id': int(),\
		'location_id': None,\
		'location_name': str(),\
		'location_ruian_id': int(),\
		'gps_lat': float(),\
		'gps_lng': float(),\
		'country': str(),\
		'region_name': str(),\
		'district_name': str(),\
		'city_name': str(),\
		'city_district_name': str(),\
		'city_part_name': str(),\
		'street_name': str(),\
		'point_name' : str(),\
		'point_zip': int(),\
		'email': str(),\
		'phone': int(),\
		'url_website': None}]

SEEN = []

HEADER = ['name', 'description', 'osm_way_id',\
		'osm_node_id',\
		'location_id',\
		'location_name',\
		'location_ruian_id',\
		'gps_lat',\
		'gps_lng',\
		'country',\
		'region_name',\
		'district_name',\
		'city_name',\
		'city_district_name',\
		'city_part_name',\
		'street_name',\
		'point_name',\
		'point_zip',\
		'email',\
		'phone',\
		'url_website']


class FromOsmToCsv:
	"""Import data from OSM to csv"""

	def __init__(self):
		self.app = Nominatim(user_agent="anonymous@gmail.com", timeout=30)
		self.count_of_lines = 0

	def run(self, default_category, country_code, category, subcategory, tag):
		"""Main starting function"""
		data_from_query = self.__overpass_query(country_code, subcategory, tag)

		if os.path.isfile('./data/{}/{}/{}_{}_osm.csv'.format(default_category, category,\
										country_code.lower(),\
										subcategory)):
			with open('./data/{}/{}/{}_{}_osm.csv'.format(default_category, category,\
									country_code.lower(),\
									subcategory), 'r') as file:
				for line in csv.reader(file):
					line = dict(zip(HEADER, line))
					DATA.append(line)
					log.debug('Was read from file line -> %s', line)

		if data_from_query:
			with open('./data/{}/{}/{}_{}_osm.csv'.format(default_category, category,\
													country_code.lower(),\
													subcategory), 'a') as file:
				writer = csv.DictWriter(file, fieldnames=HEADER)
				writer.writeheader()
				for row in data_from_query['elements']:
					line = self.__add_row(row, country_code, category, subcategory)
					if not line:
						log.err('Line was skipped -> wrong value')
						continue
					# if self.count_of_lines == DATA_LIMIT:
					# 	break
					seen = False
					for item in DATA[1:]:
						if str(item['location_name']) == str(line['location_name']):
							seen = True
							break
					self.count_of_lines += 1
					if seen:
						log.err('Line #%s was skipped -> DUPLICATE: %s', (self.count_of_lines, line))
						continue
					writer.writerow(line)
					DATA.append(line)
					seen = False
					log.debug('Was appended line #%s : %s' % (self.count_of_lines, line))

	def __add_row(self, raw_data, country_code, category, subcategory):
		"""Create csv file from JSON"""
		line = {}

		if raw_data['type'] == 'way':
			log.debug('Node type: way -> %s', raw_data)
			if raw_data.get('geometry'):
				raw_data['lat'] = raw_data['geometry'][0]['lat']
			else:
				return False

			if raw_data.get('geometry'):
				raw_data['lon'] = raw_data['geometry'][0]['lon']
			else:
				return False

			if raw_data.get('nodes'):
				raw_data['node_id'] = raw_data['nodes'][0]
			else:
				return False

		location = self.app.reverse(str(raw_data.get('lat')) + "," + str(raw_data.get('lon')))
		if not location:
			return False
		address = location.raw['address']

		if raw_data.get('tags'):
			if raw_data['tags'].get('name'):
				line['name'] = raw_data['tags']['name']
			else:
				if raw_data.get('tags').get('name:en'):
					line['name'] = raw_data['tags']['name:en']
				else:
					line['name'] = '_'.join((category, subcategory))
			line['location_ruian_id'] = raw_data['tags'].get('ref:ruian:addr')
			line['email'] = raw_data['tags'].get('email')
			phone = re.sub("[^0-9]", "", str(raw_data['tags'].get('phone')).strip())
			line['phone'] = phone
			line['url_website'] = raw_data['tags'].get('contact:website')
		else:
			line['name'] = '_'.join((category, subcategory))

		if raw_data.get('description'):
			line['description'] = raw_data['description']

		if raw_data.get('id'):
			if raw_data.get('type') == 'way':
				line['osm_way_id'] = raw_data['id']
				line['osm_node_id'] = raw_data['node_id']
			elif raw_data.get('type') == 'node':
				line['osm_node_id'] = raw_data['id']
				line['osm_way_id'] = None

		line['location_id'] = None

		if address.get('road') and address.get('house_number'):
			line['point_name'] = ' '.join((address['road'], address['house_number']))
		else:
			line['point_name'] = str()

		line['gps_lat'] = raw_data.get('lat')
		line['gps_lng'] = raw_data.get('lon')

		line['country'] = country_code.lower()

		line['region_name'] = address.get('county')
		if str(address.get('municipality')).startswith('okres'):
			line['district_name'] = str(address.get('municipality')).split()[1]

		if str(address.get('city')).startswith("Hlavn"):
			line['city_name'] = "Praha"
		else:
			line['city_name'] = address.get('city')
		line['city_district_name'] = str()
		line['city_part_name'] = address.get('suburb')
		line['street_name'] = address.get('road')

		location_names = []
		no_street = False
		if str(line['street_name']) in str(line['point_name']):
			no_street = True

		for location in ('point_name', 'street_name',\
						 'city_part_name', 'city_name',\
						 'district_name', 'region_name'):
			if location == 'city_name' and str(line.get('city_name')).lower() == 'hlavní město praha':
				location_names.append('Praha')
			if location == 'street_name' and no_street:
				continue
			if not line.get(location):
				continue
			if line.get(location) in location_names:
				continue
			location_names.append(line[location])

		line['location_name'] = ', '.join(location_names[:3])
		postcode = re.sub("[^0-9]", "", str(address.get('postcode')).strip())
		line['point_zip'] = postcode

		return line

	def __overpass_query(self, country_code, subcategory, tag):
		"""Get data by query for a certain category"""
		overpass_url = "http://overpass-api.de/api/interpreter"
		query = """[out:json];
				area["ISO3166-1"="{}"][admin_level=2];
				(
				 way["{}"="{}"](area);
				);
				out geom;
				out body;
				>;
				out skel qt;
				(._;>;);""".format(country_code,\
						tag, subcategory)

		response = {}
		log.debug('Request is started with query -> %s', query)
		# try:
		response = requests.get(overpass_url, params={'data': query}).json()
		# except:
		# 	log.err('JSONDecodeError')
		log.debug('Request is finished with response -> %s', response)
		return response


if __name__ == '__main__':
	from_osm = FromOsmToCsv()

	DEFAULT_CATEGORY = sys.argv[1]
	COUNTRY_CODE = sys.argv[2]
	CATEGORY = sys.argv[3]
	SUBCATEGORY = sys.argv[4]
	TAG = sys.argv[5]

	from_osm.run(DEFAULT_CATEGORY, COUNTRY_CODE, CATEGORY, SUBCATEGORY, TAG)
