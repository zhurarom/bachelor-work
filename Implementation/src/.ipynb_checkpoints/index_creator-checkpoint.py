#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
    Will calculate index for each point of interest
    All possible points of interests you can find here: https://www.notion.so/igluu/8d2f09b39db34a26a09142e670ff4c23?v=641b03c3bbef434d9f23a73ee0420f6b
"""

import csv
import enum
import copy
import math
import json
from tkinter import N
from tkinter.font import BOLD
from xmlrpc.client import Boolean
import pandas as pd
import psycopg2
from pymongo import database
from sklearn import preprocessing
import numpy as np
import requests
from unidecode import unidecode
import geocoder
import unicodedata
import configparser
import logging
import re
from typing import List, Tuple
from warnings import filters
from bs4 import BeautifulSoup
import pymongo
from geopy.geocoders import Nominatim

# Name of indexes
class Indexes(enum.Enum):
    """ All possible indexes """

    COFFEE = 'coffee'
    RESTAURANT = 'restaurant'
    PUB = 'pub'
    SCHOOL_ELEMENTARY = 'school_elementary'
    SCHOOL_MIDDLE = 'school_middle'
    SCHOOL_HIGH = 'school_high'
    FITNESS = 'fitness'


# cluster type
class IndexClusterTypes(enum.Enum):
    """ Cluster types """
    NEAR = 'near'
    MEDIUM = 'medium'
    FAR = 'far'


# destination by some transports
class TransportTypes(enum.Enum):
    """ Type of transport """
    WALK = 'walk'
    CAR = 'car'


# URL to get population of cities in CZ
CZ_CITY_POPULATION_URL = "https://cs.wikipedia.org/wiki/Seznam_měst_v_Česku_podle_počtu_obyvatel"
LIST_OF_INDEXES = ['coffee', 'pub', 'school_elementary',
                   'school_middle', 'school_high', 'fitness',
                   'restaurant']

# TODO: run for all properties in db
# Centrum Praha - 50.083878, 14.427035
# Pec pod Snezka - 50.727308, 15.740804
# SV. Ludmila - 50.075794, 14.435156
# Jiriho z Podebrad - 50.078013, 14.448674
GPS_LAT, GPS_LNG = 50.083878, 14.427035
AMENITY = 'fitness'

# db params
MONGO_CLIENT_NAME = 'mongodb://collab:E62sYuRZ12aO@localhost:27019/collab_stage?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&directConnection=true&ssl=false'
MONGO_DB_NAME = 'collab_stage'
MONGO_COLLECTION_NAME = 'amenities'


# Indexes_for_all_clusters:
# {'first_cluster': 32.479336864601066,
#  'second_cluster': 25.612024008704598,
#  'third_cluster': 36.05835938051803}
# For gps: (50.078013, 14.448674) and
#          amenity: fitness -> INDEX: {'index': 30.090313973834178,
#  'the_nearest_distance': 0.019090820637470286, 'minutes': 1,
#  'type': 'walk', 'count_of_activities': 9,
#  'cluster': 'near'}

class CitySizer:

    def __init__(self) -> None:
        self.app = Nominatim(user_agent="anonymous@gmail.com", timeout=30)

    def get_city_size(self, property_coordinates: tuple) -> str:
        """ Return the size of the city """

        city_size = 'small_city'
        population = self.__get_population_for_cities()
        if not population:
            return city_size

        city = self.__get_city_name_from_gps(property_coordinates)
        if not city:
            return city_size

        city_population = population.get(city)
        if not city_population:
            return city_size

        if 10000 < int(city_population) < 100000:
            city_size = 'middle_city'
        elif 100000 <= int(city_population):
            city_size = 'big_city'

        return city_size

    def __get_city_name_from_gps(self, property_coordinates: tuple) -> str:
        """ Get name of a city from gps coordinates """

        location = self.app.reverse(
            str(property_coordinates[0]) + "," + str(property_coordinates[1]))

        location = location.raw["address"]

        if location.get('city'):
            # convert to Praha for simplicity
            city = unidecode(location['city'])

            if city == 'Hlavni mesto Praha':
                city = 'Praha'

            return city

        if location.get('town'):
            town = unidecode(location['town'])

            return town

        # the town is to small, the population is less than 100 people
        return str()

    def __get_population_for_cities(self) -> dict:
        """ Get population for region where we have an appartment. Need it to improve radius in .cfg file """

        population_for_cities = {}

        res = requests.get(CZ_CITY_POPULATION_URL).text
        soup = BeautifulSoup(res, 'lxml')

        for items in soup.find('table').find_all('tr')[1::1]:
            data = items.find_all(['td'])
            try:
                country = unicodedata.normalize(
                    "NFKD", unidecode(data[1].a.text))
                population = unicodedata.normalize(
                    "NFKD", data[3].text).replace('\n', '').replace(' ', '')
            except IndexError:
                pass
            population_for_cities[country] = str(population)

        return population_for_cities


class IndexCreator:
    """ Main class, which will be included into graphQL API """

    def __init__(self, property_coordinates: tuple):

        self.the_nearest_distance = 10.0
        self.property_coordinates = property_coordinates
        self.count_of_activities = int()
        self.cluster = IndexClusterTypes.NEAR.value

        # get weights from conf file
        parser = configparser.RawConfigParser()
        parser.read('src/index_configuration.cfg')

        self.params = {}

        print(f'PARSER: {parser}')
        param_common = dict(parser.items('index_common'))

        # TODO: make tunning of parameters for different city sizes
        for index in Indexes:
            self.params[index.value] = copy.deepcopy(param_common)
            self.params[index.value].update(
                dict(parser.items('index_%s' % index.value)))

            self.params[index.value]['categories_seo'] = self.params[index.value]['categories_seo']

        param_radius = dict(parser.items(
            CitySizer().get_city_size(self.property_coordinates)))
        for index in Indexes:
            self.params[index.value] = copy.deepcopy(param_common)

            self.params[index.value].update(copy.deepcopy(param_radius))
            self.params[index.value].update(
                dict(parser.items('index_%s' % index.value)))
            self.params[index.value]['categories_seo'] = self.params[index.value]['categories_seo']

        # set up the radius for different city sizes

        print(f'PARAMS: {self.params}')

    def main(self, amenity: str) -> float:
        """ The main function to get index for some  """

        # read data from index.cfg
        params = self.params.get(amenity)

        data = self.__get_amenities_from_csv(params)
        if not data:
            index_data = {
                'index': float(),
                'the_nearest_distance': float(),
                'count_of_activities': int(),
                'cluster': str()}
            logging.info('index_clustering(index_for=%s, property_coordinates=%s)\
                     -> NO DATA IN DB, index_data=%s',
                         (amenity, self.property_coordinates, index_data))
            return index_data

        index_data = self.__calculate_final_index(params, data)
        print(f'For gps: {self.property_coordinates} and \
         amenity: {amenity} -> INDEX: {index_data}')

        logging.info('run(index_for=%s, property_coordinates=%s) -> INDEX_DATA=%s',
                     (amenity, self.property_coordinates, index_data))
        return index_data

    @staticmethod
    def __haversine(params, lng1, lat1, lng2, lat2) -> Tuple[bool, float]:
        """
            Calculate the great circle distance between two points
            on the earth (specified in decimal degrees)
        """

        # convert decimal degrees to radians
        lng1, lat1, lng2, lat2 = map(
            math.radians, [lng1, lat1, lng2, lat2])

        # haversine formula
        dlng = lng2 - lng1
        dlat = lat2 - lat1
        first = math.sin(dlat / 2) ** 2 + math.cos(lat1) * \
            math.cos(lat2) * math.sin(dlng / 2) ** 2
        second = 2 * math.asin(math.sqrt(first))
        earth_radius = 6371  # Radius of earth in kilometers. Use 3956 for miles
        distance_km = second * earth_radius

        if distance_km > float(params.get('radius_third_cluster')):
            logging.debug('__haversine(params=%s) -> is_NOT_inside', params)
            return False, distance_km

        logging.debug('__haversine(params=%s) -> is_inside', params)
        return True, distance_km

    # TODO: also add filter for category
    def __get_amenities_from_csv(self, params: dict) -> list:
        """
            Get amenities from mongo csv in a range (radius of the third cluster).
            We need it because, we don`t want to get all amenities from db
        """

        property_lng, property_lat = self.property_coordinates[1], self.property_coordinates[0]

        # get filters from .cfg file
        filters = params.get('categories_seo').split('-')

        # if we don`t have sub_sub_category, write down empty string to it
        if len(filters) == 2:
            filters.append('')

        # read csv file with amenities as list of dicts
        raw_amenities = []
        with open("indexer/data/amenities.csv", "r") as f:
            csv_reader = csv.DictReader(f)
            raw_amenities = list(csv_reader)

        print(f'LEN BEFORE: {len(raw_amenities)}')
        print(f'FILTERS: {filters}')

        # Take amenities only with our category
        amenities = []
        for amenity in raw_amenities:
            if amenity['category'] == filters[0] and \
                    amenity['sub_category'] == filters[1] and \
                    amenity['sub_sub_category'] == filters[2]:
                amenities.append(amenity)

        print(f'LEN AFTER FILTERING: {len(amenities)}')

        amenities_inside_cluster = []
        for amenitity in amenities:
            amenity_lng, amenitity_lat = eval(amenitity.get('address.coordinates'))[0], \
                eval(amenitity.get('address.coordinates'))[1]

            is_inside, distance = self.__haversine(
                params, property_lng, property_lat, amenity_lng, amenitity_lat)

            if not is_inside:
                continue

            amenitity.update({'distance': distance})
            # print(f'AMENITY: {amenitity}')
            amenities_inside_cluster.append(amenitity)

        print(f'LEN AFTER: {len(amenities_inside_cluster)}')

        if not amenities_inside_cluster:
            logging.debug('__get_amenities_from_csv(param=%s, property_coordinates=%s) -> no amenities',
                          (params, self.property_coordinates))
            return amenities_inside_cluster

        logging.debug('__get_amenities_from_csv(params=%s, property_coordinates=%s)\
                        -> amenities=%s, amenitites_len=%s',
                      (params, self.property_coordinates, amenities_inside_cluster, len(amenities_inside_cluster)))
        return amenities_inside_cluster

    def __get_amenities_from_db(self, params: dict) -> List:
        """
            Get amenities from mongo DB in a range (radius of the third cluster).
            We need it because, we don`t want to get all amenities from db
        """

        # get filters from .cfg file
        filters = params.get('categories_seo').split('-')

        # if we don`t have sub_sub_category, write down empty string to it
        if len(filters) == 2:
            filters.append('')

        # connect to mongo db
        client = pymongo.MongoClient(MONGO_CLIENT_NAME)
        db = client[MONGO_DB_NAME]
        collection = db[MONGO_COLLECTION_NAME]

        cursor = collection.aggregate([
            {
                '$geoNear': {
                    'near': {'type': 'Point', 'coordinates': [self.property_coordinates[1], self.property_coordinates[0]]},
                    'distanceField': 'distance',
                    'maxDistance': float(params.get('radius_third_cluster')) * 1000,
                    'spherical': True,
                }
            },
            {'$match': {
                'category': filters[0], 'sub_category': filters[1], 'sub_sub_category': filters[2]}}
        ])

        amenities = [row for row in cursor]
        if not amenities:
            logging.debug('__get_amenities_from_db(param=%s, property_coordinates=%s) -> no amenities',
                          (params, self.property_coordinates))
            return amenities

        print(f'AMENITIES: {amenities}')
        logging.debug('__get_amenities_from_db(params=%s, property_coordinates=%s)\
                        -> amenities=%s, amenitites_len=%s',
                      (params, self.property_coordinates, amenities, len(amenities)))
        return amenities

    @staticmethod
    def __minutes_to_walk(distance: float, walked_km_per_hour: float) -> int():
        """Convert distance to minutes for walking"""

        walked_minutes = int(distance * 60.0 // walked_km_per_hour)
        if walked_minutes == 0:
            logging.debug('__minutes_to_walk(distance=%s, walked_km_per_hour=%s) -> walked_minutes=1',
                          (distance, walked_km_per_hour, walked_minutes))
            return 1

        logging.debug('__minutes_to_walk(distance=%s, walked_km_per_hour=%s) -> walked_minutes=%s',
                      (distance, walked_km_per_hour, walked_minutes))
        return walked_minutes

    @staticmethod
    def __index_for_one_activity(params: dict, current_distance: float) -> float:
        """Calculate index for one activity"""

        # Make kind of normalization for distance
        radius = float(params['radius'])
        radius_second_cluster = float(params['radius_second_cluster'])
        max_index_for_max_distance = float(
            params['max_index_for_max_distance'])
        max_possible_distance_for_index = float(
            params['max_possible_distance_for_index'])

        if radius < current_distance < radius_second_cluster:
            current_distance = current_distance / radius
        elif current_distance > radius_second_cluster:
            current_distance = current_distance / radius_second_cluster
        index_for_one_activity = current_distance * \
            max_index_for_max_distance / max_possible_distance_for_index

        logging.debug('__index_for_one_activity(current_distance=%s) -> index=%s',
                      (current_distance, index_for_one_activity))
        return index_for_one_activity

    def __count_of_activities_for_one_from_three_clusters(self, three_cluster_distances):
        """Write down the count of activity for the nearest cluster"""

        first = len(three_cluster_distances['first_cluster'])
        second = len(three_cluster_distances['second_cluster'])
        third = len(three_cluster_distances['third_cluster'])

        if first == 0:
            if second == 0:
                self.count_of_activities = third
                self.cluster = IndexClusterTypes.FAR.value
            else:
                self.count_of_activities = second
                self.cluster = IndexClusterTypes.MEDIUM.value
        else:
            self.count_of_activities = first

        logging.debug('Count of data inside the near cluster -> %s', first)
        logging.debug('Count of data inside the medium cluster -> %s', second)
        logging.debug('Count of data inside the far cluster -> %s', third)

    def __calculate_distances_for_clusters(self, data: dict, params: dict) -> dict:
        """ Calculate distances for one of three clusters """

        three_cluster_distances = {
            'first_cluster': [],
            'second_cluster': [],
            'third_cluster': []}

        count = 0
        radius = float(params['radius'])
        radius_second_cluster = float(params['radius_second_cluster'])
        radius_third_cluster = float(params['radius_third_cluster'])

        for row in data:

            # set up the correct cluster for the single activity
            cluster_name = 'first_cluster'
            distance = row.get('distance')
            gps_lng, gps_lat = self.property_coordinates[1], self.property_coordinates[0]

            # gps_lat = row.get('address').get('coordinates')[1]
            # gps_lng = row.get('address').get('coordinates')[0]

            # is inside the ring between circle and the second cluster
            if radius <= distance <= radius_second_cluster:
                cluster_name = 'second_cluster'

            # is inside the ring between the second cluster and the third cluster
            if radius_second_cluster <= distance <= radius_third_cluster:
                cluster_name = 'third_cluster'

            if distance < self.the_nearest_distance:
                self.the_nearest_distance = distance

            # is inside of a circle
            activity_inside = {
                'distance': distance,
                'gps_lng': gps_lng,
                'gps_lat': gps_lat}

            if cluster_name == 'first_cluster':
                three_cluster_distances['first_cluster'].append(
                    activity_inside)
                count += 1
                logging.debug(f'Activity for first_cluster: osm_node_id=%s, gps_lat and gps_lng: %s, %s',
                              (row.get('source_id'), gps_lat, gps_lng))

            # is inside the ring between circle and the second cluster
            elif cluster_name == 'second_cluster':
                three_cluster_distances['second_cluster'].append(
                    activity_inside)
                count += 1
                logging.debug(f'Activity for second_cluster: osm_node_id=%s, gps_lat and gps_lng: %s, %s',
                              (row.get('source_id'), gps_lat, gps_lng))

            # is inside the ring between the second cluster and the third cluster
            elif cluster_name == 'third_cluster':
                three_cluster_distances['third_cluster'].append(
                    activity_inside)
                count += 1
                logging.debug(f'Activity for third_cluster: osm_node_id=%s, gps_lat and gps_lng: %s, %s',
                              (row.get('source_id'), gps_lat, gps_lng))

        logging.debug('Count of data inside all clusters -> %s', count)
        self.__count_of_activities_for_one_from_three_clusters(
            three_cluster_distances)

        logging.debug('__calculate_distances(gps_lng=%s, gps_lat=%s, data=%s) -> three_cluster_distances=%s',
                      (gps_lng, gps_lat, data, three_cluster_distances))
        return three_cluster_distances

    def __calculate_single_index(self, params: dict, data: List) -> None:
        """ Calculate single index for each amenity """

        distances = self.__calculate_distances_for_clusters(data, params)

        indexes_for_all_clusters = {
            'first_cluster': float(),
            'second_cluster': float(),
            'third_cluster': float()}

        for cluster in distances.keys():

            single_indexes = []
            distance_before = 0.0
            count_of_the_same_distances = 0.0
            index_for_one_cluster = 0.0

            logging.debug('====== INDEX FOR CLUSTER=%s ======', cluster)

            for activity in distances[cluster]:

                if not activity:
                    indexes_for_all_clusters[cluster] = 0.0
                    logging.debug('__calculate_index_for_point(distances=%s) -> index=%s',
                                  (distances, indexes_for_all_clusters[cluster]))
                    # return indexes_for_all_clusters
                    break

                if len(activity) == 1:
                    index_for_point = self.__index_for_one_activity(
                        params,
                        activity.get('distance')) * 0.4 * 100
                    indexes_for_all_clusters[cluster] = index_for_point
                    logging.debug('__calculate_index_for_point(distances=%s) -> index_for_point=%s',
                                  (distances, index_for_point))
                    # return indexes_for_all_clusters
                    break

                index_for_one_activity = self.__index_for_one_activity(
                    params, activity.get('distance'))
                index_for_single_activity = 1 - index_for_one_activity

                if len(distances[cluster]) >= 150:
                    index_for_single_activity += float(
                        params['bonus_for_count_of_activities'])

                # if distance between activities is not greater, than 0.1 km -> index_activity_1 * 0.8,
                # index_activity_2 * (0.8 + 0.1) + ...
                if math.fabs(activity.get('distance') - distance_before) <= 0.1:
                    count_of_the_same_distances += 0.1
                    index_for_single_activity = index_for_one_activity +\
                        float(params['max_index_for_max_distance']) + \
                        count_of_the_same_distances

                # convert negative value
                index_for_single_activity = abs(index_for_single_activity)

                logging.debug('Index for single activity: %s',
                              (index_for_single_activity))
                single_indexes.append(index_for_single_activity)

            if single_indexes:
                # TODO: fix count of activities
                index_for_one_cluster = sum(
                    single_indexes) / len(single_indexes) * 100

                indexes_for_all_clusters[cluster] = index_for_one_cluster
                logging.debug('Index for cluster=%s -> index=%s',
                              (cluster, index_for_one_cluster))

        # indexes normalization
        # TODO: check if it is correct
        normalized_indexes_for_all_clusters = {}

        for key, value in indexes_for_all_clusters.items():
            normalized_value = value % 100
            normalized_indexes_for_all_clusters[key] = normalized_value

        logging.info('__calculate_index_for_point() -> normalized_indexes_for_all_clusters=%s',
                     (normalized_indexes_for_all_clusters))
        return normalized_indexes_for_all_clusters

    def __calculate_final_index(self, params: dict, data: List) -> None:
        """ Calculate final index from single ones """

        indexes_for_all_clusters = self.__calculate_single_index(params, data)
        print(f'Indexes_for_all_clusters: {indexes_for_all_clusters}')

        # set up normal default value
        if self.count_of_activities == 0:
            self.the_nearest_distance = 0.0

        # TODO: replace DestinationBy on different types, car, bike etc
        index_data = {
            'index': float(),
            'the_nearest_distance': self.the_nearest_distance,
            'minutes': self.__minutes_to_walk(
                self.the_nearest_distance,
                float(params['walked_km_per_hour'])),
            'type': TransportTypes.WALK.value,
            'count_of_activities': self.count_of_activities,
            'cluster': self.cluster}

        final_index = indexes_for_all_clusters['first_cluster'] *\
            float(params['coefficient_for_the_first_cluster']) +\
            indexes_for_all_clusters['second_cluster'] *\
            float(params['coefficient_for_the_second_cluster']) +\
            indexes_for_all_clusters['third_cluster'] *\
            float(params['coefficient_for_the_third_cluster'])
        index_data['index'] = final_index

        logging.debug('__calculate_final_index_for_clusters_indexes(indexes_for_all_clusters=%s)\
                -> index_data=%s', (indexes_for_all_clusters, index_data))
        return index_data


# def get_gps_from_listing_id(listing_id: str) -> tuple:
#     """ Get gps from listing_id from postgress """

#     connection = None
#     try:
#         connection = psycopg2.connect(
#             user="sysadmin",
#             password="pynative@#29",
#             host="127.0.0.1",
#             port="5432",
#             database="postgres_db")
#         cursor = connection.cursor()
#         postgreSQL_select_Query = "select * from mobile where listingId=%s" % \
#             (listing_id)

#         cursor.execute(postgreSQL_select_Query)
#         print("Selecting rows from mobile table using cursor.fetchall")
#         mobile_records = cursor.fetchall()

#         print("Print each row and it's columns values")
#         print(f'Mobile records: {mobile_records}')
#         # for row in mobile_records:
#         # print("Price  = ", row[2], "\n")

#     except (Exception, psycopg2.Error) as error:
#         print("Error while fetching data from PostgreSQL", error)

#     finally:
#         # closing database connection.
#         if connection:
#             cursor.close()
#             connection.close()
#             print("PostgreSQL connection is closed")


# TODO: find the way how to deploy lambda
# GET: /{listing_id}/indexer


def lambda_handler(event=None, context=None):
    """ AWS lambda fucntion """

    # get gps from listing_idsls
    # if not event:
    #     listing_id = 'EU-CZ-900780492'
    # else:
    #     listing_id = event['queryStringParameters'].get('listing_id')
    #     if not listing_id:
    #         return {}

    # property_coordinates = get_gps_from_listing_id(listing_id)
    property_coordinates = ()

    data = {}

    for index_name in LIST_OF_INDEXES:
        NEW_INDEX = IndexCreator(property_coordinates)
        NEW_INDEX.main(index_name)
        data[index_name] = NEW_INDEX

    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = 'application/json'
    responseObject['body'] = json.dumps(data)

    return responseObject


# TODO: add cron
# TODO: save indexes for all listings in db
if __name__ == '__main__':

    property_coordinates = (GPS_LAT, GPS_LNG)
    NEW_INDEX = IndexCreator(property_coordinates)

    NEW_INDEX.main(AMENITY)
    # lambda_handler()
    # NEW_INDEX.get_city_name_from_gps(property_coordinates)
    # print(__get_population_for_cities())
