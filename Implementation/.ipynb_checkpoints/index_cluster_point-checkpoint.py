#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Index creating
"""

# pylint: disable=too-few-public-methods
# pylint: disable=fixme

import copy
import math
import configparser
from typing import Tuple
from geopy.geocoders import Nominatim

from logni import log
from model.base import DML
from model.datapi import ModelDatapi
from model.category import ModelDataCategory
import enum4


# TESTING DATA FOR COFFEE
# Na Petrinach: 50.0881386 14.342946 == 59.91
# Stare Mesto: 50.0819554 14.4147994 == 80.30
# Vaclavak: 50.0826857 14.4286876 == 79.38
# Seifertova: 50.0839191 14.4492754 == 72.67
# DEJVICKA NTK: 50.1038462 14.3893944 == 68.41
# NO CAFE: 50.0176494 14.6460303 == 18.0097
# Six: 50.092096 14.3237718 == 43.58
# One cafe: 50.0946357 14.3057285 == 40.26
# CENTER: 50.0883625 14.4184825 == 78.82
# OKRAJINA - BUTOVICE: 49.9742445 14.1700008 == 19.49
# Blizko Snezky: 50.7534701 15.6949199 == 4.53
# OKRAJINA: 50.3310684 14.4864238


class CreateIndex:
	"""Index creating"""

	def __init__(self):

		self.app = Nominatim(user_agent="anonymous@gmail.com", timeout=30)
		self.the_nearest_distance = 10
		self.count_of_activities = int()
		self.cluster = enum4.IndexClusterTypes.NEAR.value

		parser = configparser.RawConfigParser()
		parser.read('etc/index.cfg')

		self.params = {}
		param_common = dict(parser.items('index_common'))

		for index in enum4.Indexes:
			self.params[index.value] = copy.deepcopy(param_common)
			self.params[index.value].update(dict(parser.items('index_%s' % index.value)))

			self.params[index.value]['categories_seo'] = \
				tuple(self.params[index.value]['categories_seo'].split(','))

	def run(self, index_enum, gps_lat: float, gps_lng: float) -> dict:
		"""Main function for index creating"""

		# read data from index.cfg
		config_params = self.params[index_enum.value]

		categories_seo = config_params['categories_seo']
		category_ids = self.__get_category_ids(config_params['categories_seo'])

		# category_id doesn't exist
		if not category_ids:
			log.error('run(index_for=%s, gps_lng=%s, gps_lat=%s) -> CATEGORY_ID DOESN`T EXIST',\
								(categories_seo, gps_lng, gps_lat))
			return {}

		data = self.__get_data_for_certain_city(config_params, category_ids, gps_lng, gps_lat)
		if not data:
			index_data = {\
					'index': float(),
					'the_nearest_distance': float(),
					'count_of_activities': int(),
					'cluster': str()}
			log.error('run(index_for=%s, gps_lng=%s, gps_lat=%s) -> NO DATA IN DB, index_data=%s', \
				(categories_seo, gps_lng, gps_lat, index_data))
			return index_data

		distances = self.__calculate_distances(config_params, gps_lng, gps_lat, data)
		indexes_for_all_clusters = self.__calculate_index_for_point(config_params, distances)
		index_data = self.__calculate_final_index_for_clusters_indexes(config_params,\
									indexes_for_all_clusters)

		log.info('run(index_for=%s, gps_lng=%s, gps_lat=%s) -> INDEX_DATA=%s',\
						(categories_seo, gps_lng, gps_lat, index_data))
		return index_data

	@staticmethod
	def __minutes_to_walk(distance: float, walked_km_per_hour: float) -> int():
		"""Convert distance to minutes for walking"""

		walked_minutes = int(distance * 60.0 // walked_km_per_hour)
		if walked_minutes == 0:
			log.info('__minutes_to_walk(distance=%s, walked_km_per_hour=%s) -> walked_minutes=1', \
				 (distance, walked_km_per_hour, walked_minutes))
			return 1

		log.info('__minutes_to_walk(distance=%s, walked_km_per_hour=%s) -> walked_minutes=%s', \
			 (distance, walked_km_per_hour, walked_minutes))
		return walked_minutes

	def __calculate_final_index_for_clusters_indexes(self, config_params: dict,\
							 indexes_for_all_clusters: dict) -> dict:
		"""Calculate final index from three clusters indexes"""

		# set up normal default value
		if self.count_of_activities == 0:
			self.the_nearest_distance = 0.0

		# TODO: replace DestinationBy on different types
		index_data = {\
				'index': float(),
				'the_nearest_distance': self.the_nearest_distance,
				'minutes': self.__minutes_to_walk(\
							self.the_nearest_distance,
							float(config_params['walked_km_per_hour'])),
				'type': enum4.TransportTypes.WALK.value,
				'count_of_activities': self.count_of_activities,
				'cluster': self.cluster}

		final_index = indexes_for_all_clusters['first_cluster'] *\
						float(config_params['coefficient_for_the_first_cluster']) +\
				indexes_for_all_clusters['second_cluster'] *\
						float(config_params['coefficient_for_the_second_cluster']) +\
				indexes_for_all_clusters['third_cluster'] *\
						float(config_params['coefficient_for_the_third_cluster'])
		index_data['index'] = final_index

		log.info('__calculate_final_index_for_clusters_indexes(indexes_for_all_clusters=%s)\
					-> index_data=%s', (indexes_for_all_clusters, index_data))
		return index_data

	@staticmethod
	def __get_category_ids(categories_seo) -> list:
		"""Get category_ids from house4data in category table by seo name"""

		category_indexes = []
		for category_seo in categories_seo:
			res = DML.query(ModelDataCategory).filter(ModelDataCategory.seo == category_seo).first()

			if not res:
				log.error('__get_category_ids(index_for=%s) = doesn`t exist', category_indexes)
				return []

			category_indexes.append(res.row_id)

		log.info('__get_category_id(index_for=%s) = index=%s', (categories_seo, category_indexes))
		return category_indexes

	@staticmethod
	def __index_for_one_activity(config_params: dict, current_distance: float) -> float:
		"""Calculate index for one activity"""

		# Make kind of normalization for distance
		radius = int(config_params['radius'])
		radius_second_cluster = int(config_params['radius_second_cluster'])
		max_index_for_max_distance = float(config_params['max_index_for_max_distance'])
		max_possible_distance_for_index = float(config_params['max_possible_distance_for_index'])

		if radius < current_distance < radius_second_cluster:
			current_distance = current_distance / radius
		elif current_distance > radius_second_cluster:
			current_distance = current_distance / radius_second_cluster
		index_for_one_activity = current_distance * max_index_for_max_distance /\
						max_possible_distance_for_index

		log.debug('__index_for_one_activity(current_distance=%s) -> index=%s',\
							(current_distance, index_for_one_activity))
		return index_for_one_activity

	def __calculate_index_for_point(self, config_params: dict, distances: dict) -> dict:
		"""Calculate index for point"""

		indexes_for_all_clusters = {'first_cluster': float(),\
					'second_cluster': float(),\
					'third_cluster': float()}

		for cluster in distances.keys():

			single_indexes = []
			distance_before = 0.0
			count_of_the_same_distances = 0.0
			index_for_one_cluster = 0.0

			log.info('====== INDEX FOR CLUSTER=%s ======', cluster)

			for activity in distances[cluster]:

				if not activity:
					indexes_for_all_clusters[cluster] = 0.0
					log.info('__calculate_index_for_point(distances=%s) -> index=%s', \
						(distances, indexes_for_all_clusters[cluster]))
					# return indexes_for_all_clusters
					break

				if len(activity) == 1:
					index_for_point = self.__index_for_one_activity(config_params,\
								activity.get('distance')) * 0.4 * 100
					indexes_for_all_clusters[cluster] = index_for_point
					log.info('__calculate_index_for_point(distances=%s) -> index_for_point=%s', \
						(distances, index_for_point))
					# return indexes_for_all_clusters
					break

				index_for_one_activity = self.__index_for_one_activity(config_params, activity.get('distance'))
				index_for_single_activity = 1 - index_for_one_activity

				if len(distances[cluster]) >= 10:
					index_for_single_activity += float(config_params['bonus_for_count_of_activities'])

				# if distance between activities is not greater, than 0.1 km -> index_activity_1 * 0.8,
				# index_activity_2 * (0.8 + 0.1) + ...
				if math.fabs(activity.get('distance') - distance_before) <= 0.1:
					count_of_the_same_distances += 0.1
					index_for_single_activity = index_for_one_activity +\
							float(config_params['max_index_for_max_distance']) + \
							count_of_the_same_distances

				# convert negative value
				index_for_single_activity = abs(index_for_single_activity)

				log.info('Index for single activity: %s', (index_for_single_activity))
				single_indexes.append(index_for_single_activity)

			if single_indexes:
				index_for_one_cluster = sum(single_indexes) / len(single_indexes) * 100
			indexes_for_all_clusters[cluster] = index_for_one_cluster
			log.info('Index for cluster=%s -> index=%s', (cluster, index_for_one_cluster))

		log.info('__calculate_index_for_point() -> indexes_for_all_clusters=%s',\
							(indexes_for_all_clusters))
		return indexes_for_all_clusters

	def __count_of_activities_for_one_from_three_clusters(self, three_cluster_distances):
		"""Write down the count of activity for the nearest cluster"""

		first = len(three_cluster_distances['first_cluster'])
		second = len(three_cluster_distances['second_cluster'])
		third = len(three_cluster_distances['third_cluster'])

		if first == 0:
			if second == 0:
				self.count_of_activities = third
				self.cluster = enum4.IndexClusterTypes.FAR.value
			else:
				self.count_of_activities = second
				self.cluster = enum4.IndexClusterTypes.MEDIUM.value
		else:
			self.count_of_activities = first

		log.info('Count of data inside the near cluster -> %s', first)
		log.info('Count of data inside the medium cluster -> %s', second)
		log.info('Count of data inside the far cluster -> %s', third)

	@staticmethod
	def __haversine(config_params: dict, lng1, lat1, lng2, lat2) -> Tuple[str, float]:
		"""
		Calculate the great circle distance between two points
		on the earth (specified in decimal degrees)
		"""

		# convert decimal degrees to radians
		lng1, lat1, lng2, lat2 = map(math.radians, [lng1, lat1, lng2, lat2])

		# haversine formula
		dlng = lng2 - lng1
		dlat = lat2 - lat1
		first = math.sin(dlat / 2) ** 2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlng / 2) ** 2
		second = 2 * math.asin(math.sqrt(first))
		earth_radius = 6371  # Radius of earth in kilometers. Use 3956 for miles
		distance_km = second * earth_radius

		radius = int(config_params['radius'])
		radius_second_cluster = int(config_params['radius_second_cluster'])
		radius_third_cluster = int(config_params['radius_third_cluster'])

		# is inside of a circle
		if distance_km <= radius:
			log.debug('__haversine(lng1=%s, lat1=%s, lng2=%s, lat2=%s) -> inside circle, distance=%s', \
				 (lng1, lat1, lng2, lat2, distance_km))
			return 'first_cluster', distance_km

		# is inside the ring between circle and the second cluster
		if radius <= distance_km <= radius_second_cluster:
			log.debug('__haversine(lng1=%s, lat1=%s, lng2=%s, lat2=%s) ->\
			 		inside the first ring, distance=%s',\
					(lng1, lat1, lng2, lat2, distance_km))
			return 'second_cluster', distance_km

		# is inside the ring between the second cluster and the third cluster
		if radius_second_cluster <= distance_km <= radius_third_cluster:
			log.debug('__haversine(lng1=%s, lat1=%s, lng2=%s, lat2=%s) ->\
						inside the second ring, distance=%s', \
						(lng1, lat1, lng2, lat2, distance_km))
			return 'third_cluster', distance_km

		log.debug('__haversine(lng1=%s, lat1=%s, lng2=%s, lat2=%s) -> is_not_inside', \
			 (lng1, lat1, lng2, lat2))
		return '', distance_km

	def __calculate_distances(self, config_params: dict,\
				gps_lng: float, gps_lat: float, data: list) -> dict:
		"""Calculate distances from center point inside a circle"""

		three_cluster_distances = {'first_cluster': [],\
					'second_cluster': [],\
					'third_cluster': []}

		count = 0

		for row in data:
			activity_gps_lat = row.gps_lat
			activity_gps_lng = row.gps_lng
			res, distance = self.__haversine(config_params,\
							 gps_lng,\
							 gps_lat,\
							 activity_gps_lng,\
							 activity_gps_lat)

			if distance < self.the_nearest_distance:
				self.the_nearest_distance = distance

			# is inside of a circle
			activity_inside = {'distance': distance,\
						'gps_lng': activity_gps_lng,\
						'gps_lat': activity_gps_lat}

			if res == 'first_cluster':
				three_cluster_distances['first_cluster'].append(activity_inside)
				count += 1

			# is inside the ring between circle and the second cluster
			elif res == 'second_cluster':
				three_cluster_distances['second_cluster'].append(activity_inside)
				count += 1

			# is inside the ring between the second cluster and the third cluster
			elif res == 'third_cluster':
				three_cluster_distances['third_cluster'].append(activity_inside)
				count += 1

		log.info('Count of data inside all clusters -> %s', count)
		self.__count_of_activities_for_one_from_three_clusters(three_cluster_distances)

		log.info('__calculate_distances(gps_lng=%s, gps_lat=%s, data=%s) -> three_cluster_distances=%s',\
								(gps_lng, gps_lat, data, three_cluster_distances))
		return three_cluster_distances

	@staticmethod
	def __get_gps_for_square_area(gps_lat, gps_lng, degree_for_one_km, radius_third_cluster) -> dict:
		"""Get top/bottom | left/right gps coordinates for sqaure"""

		gps_radius = degree_for_one_km * radius_third_cluster

		gps_square = {'top_right': {'lat': str(), 'lng': str()},\
				'top_left': {'lat': str(), 'lng': str()},\
				'bottom_right': {'lat': str(), 'lng': str()},\
				'bottom_left': {'lat': str(), 'lng': str()}}

		gps_square['top_right']['lat'] = float(gps_lat) + gps_radius
		gps_square['top_right']['lng'] = float(gps_lng) + gps_radius

		gps_square['top_left']['lat'] = float(gps_lat) + gps_radius
		gps_square['top_left']['lng'] = float(gps_lng) - gps_radius

		gps_square['bottom_right']['lat'] = float(gps_lat) - gps_radius
		gps_square['bottom_right']['lng'] = float(gps_lng) + gps_radius

		gps_square['bottom_left']['lat'] = float(gps_lat) - gps_radius
		gps_square['bottom_left']['lng'] = float(gps_lng) - gps_radius

		log.info('__get_gps_for_square_area(gps_lat=%s, gps_lng=%s,\
			degree_for_one_km=%s, radius_third_cluster=%s) -> gps_square=%s',\
			 (gps_lat, gps_lng, degree_for_one_km, radius_third_cluster, gps_square))
		return gps_square

	def __get_data_for_certain_city(self, config_params: dict, category_ids: list,\
					gps_lng: float, gps_lat: float) -> list:
		"""Get data for a certain city"""

		degree_for_one_km = float(config_params['degree_for_one_km'])
		radius_third_cluster = int(config_params['radius_third_cluster'])

		gps_square = self.__get_gps_for_square_area(gps_lat, gps_lng,\
					degree_for_one_km, radius_third_cluster)

		# Check range for top right/left and bottom right/left
		query = DML.query(ModelDatapi).filter(ModelDatapi.category_id.in_(category_ids),\
				ModelDatapi.gps_lng <= gps_square['top_right']['lng'],\
				ModelDatapi.gps_lat <= gps_square['top_right']['lat'],\
				ModelDatapi.gps_lng >= gps_square['top_left']['lng'],\
				ModelDatapi.gps_lat <= gps_square['top_left']['lat'],\
				ModelDatapi.gps_lng <= gps_square['bottom_right']['lng'],\
				ModelDatapi.gps_lat >= gps_square['bottom_right']['lat'],\
				ModelDatapi.gps_lng >= gps_square['bottom_left']['lng'],\
				ModelDatapi.gps_lat >= gps_square['bottom_left']['lat']).all()

		if not query:
			log.error('__get_data_for_certain_city(category_ids=%s) -> NO DATA', category_ids)
			return query

		log.info('Count of data ia a square -> %s', len(query))
		log.info('__get_data_for_certain_city(category_id=%s) ->\
			data=%s', (category_ids, query))
		return query


# if __name__ == '__main__':
#
# 	activity_index = CreateIndex()
#
# 	INDEX_FOR = ('school-elementary', 'school-high')
# 	GPS_LAT = 50.092096
# 	GPS_LNG = 14.3237718
#
# 	activity_index.run(INDEX_FOR, GPS_LAT, GPS_LNG)
